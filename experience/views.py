import json
from datetime import datetime

from django.conf import settings
from django.urls import reverse_lazy
from django.views.generic import FormView, TemplateView

import web3

from .forms import AddJobForm
from .models import JobOffer, Company


class ConnectMixin:

    def connect(self, contract):
        w3 = web3.Web3(web3.HTTPProvider('http://localhost:8545'))
        with open(settings.FILEJSON_PATH) as f:
            info_json = json.load(f)
        abi = info_json['abi']
        contract = web3.main.Web3.toChecksumAddress(contract)
        escrow = w3.eth.contract(address=contract, abi=abi)
        return escrow


class AddJobFormView(ConnectMixin, FormView):

    template_name = "add_job.html"
    form_class = AddJobForm
    success_url = reverse_lazy("home")

    def get_initial(self):
        initial = super().get_initial()
        user = self.request.user
        if user.is_authenticated:
            if user.contract:
                initial['contract'] = user.contract
            if user.public_key:
                initial['address'] = user.public_key
        return initial

    def get_context_data(self, **kwargs):
        user = self.request.user
        kwargs['is_authorized'] = user.is_authenticated and \
                                  user.groups.filter(name="RH").exists()
        return super().get_context_data(**kwargs)

    def post(self, request, *args, **kwargs):
        address = request.POST.get('address', None)
        job = request.POST.get('job', None)
        start_date = request.POST.get('start_date', None)
        end_date = request.POST.get('end_date', None)
        company = request.POST.get('company', None)
        contract_address = request.POST.get('contract', None)

        job_name = JobOffer.objects.get(pk=job).name
        company_name = Company.objects.get(pk=company).name

        start_time = int(datetime.strptime(start_date, '%d/%m/%Y').timestamp())
        end_time = int(datetime.strptime(end_date, '%d/%m/%Y').timestamp())

        connect = self.connect(contract_address)

        transac = connect.functions.addJobForUser(
            address, job_name, start_time, end_time, company_name).transact(
            {'from': address}
        )
        return super().post(request, *args, **kwargs)


class ShowJobView(ConnectMixin, TemplateView):

    template_name = "show_job.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        user = self.request.user
        if user.is_authenticated:
            connect = self.connect(user.contract)
            context['call'] = connect.functions.getJobForUser(user.public_key, 0).call()
        return context


class ListJobView(ConnectMixin, TemplateView):

    template_name = "list_job.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        user = self.request.user
        if user.is_authenticated:
            connect = self.connect(user.contract)
            accounts = connect.functions.getAccounts().call()
            context['accounts'] = []
            for i, account in enumerate(accounts):
                context['accounts'].append({'member': None, 'jobs': []})
                member = connect.functions.getUser(account).call()
                job = connect.functions.getJobForUser(account, i).call()
                if job == ["", 0, 0, ""]:
                    break
                context['accounts'][i]['member'] = member
                if job:
                    start = job[1]
                    if start != 0:
                        start = datetime.fromtimestamp(start)
                    end = job[2]
                    if end != 0:
                        end = datetime.fromtimestamp(end)
                    exp = [job[0], start, end, job[3]]
                    context['accounts'][i]['jobs'].append(exp)
        return context
