# Generated by Django 2.2.4 on 2019-08-29 10:08

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Company',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False)),
                ('name', models.CharField(max_length=50, verbose_name='nom')),
                ('address', models.CharField(blank=True, max_length=50, null=True, verbose_name='address')),
            ],
            options={
                'verbose_name': 'entreprise',
                'verbose_name_plural': 'entreprises',
                'default_related_name': 'company',
            },
        ),
        migrations.CreateModel(
            name='Project',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False)),
                ('name', models.CharField(max_length=50, verbose_name='nom')),
                ('description', models.TextField(blank=True, null=True, verbose_name='description')),
            ],
            options={
                'verbose_name': 'projet',
                'verbose_name_plural': 'projets',
                'default_related_name': 'project',
            },
        ),
        migrations.CreateModel(
            name='TrainingCenter',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False)),
                ('name', models.CharField(max_length=50, verbose_name='nom')),
                ('address', models.CharField(blank=True, max_length=50, null=True, verbose_name='address')),
            ],
            options={
                'verbose_name': 'centre de formation',
                'verbose_name_plural': 'centres de formation',
                'default_related_name': 'training_center',
            },
        ),
        migrations.CreateModel(
            name='Training',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False)),
                ('name', models.CharField(max_length=50, verbose_name='nom')),
                ('certified', models.BooleanField(default=False, verbose_name='certifié')),
                ('employee', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='training', to=settings.AUTH_USER_MODEL, verbose_name='employé')),
                ('training_center', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='training', to='experience.TrainingCenter', verbose_name='centre de formation')),
            ],
            options={
                'verbose_name': 'formation',
                'verbose_name_plural': 'formations',
                'default_related_name': 'training',
            },
        ),
        migrations.CreateModel(
            name='JobOffer',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False)),
                ('name', models.CharField(max_length=50, verbose_name='nom')),
                ('description', models.TextField(blank=True, null=True, verbose_name='description')),
                ('company', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='job_offer', to='experience.Company', verbose_name='entreprise')),
            ],
            options={
                'verbose_name': "offre d'emploi",
                'verbose_name_plural': "offres d'emploi",
                'default_related_name': 'job_offer',
            },
        ),
        migrations.CreateModel(
            name='Job',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False)),
                ('name', models.CharField(max_length=50, verbose_name='nom')),
                ('company', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='job', to='experience.Company', verbose_name='entreprise')),
            ],
            options={
                'verbose_name': 'emploi',
                'verbose_name_plural': 'emplois',
                'default_related_name': 'job',
            },
        ),
    ]
