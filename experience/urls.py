from django.urls import path, re_path

import experience.views as views

urlpatterns = [
    re_path(r'^ajout-job/', views.AddJobFormView.as_view(),
            name="add_job"),
    re_path(r'^visualisation-job/', views.ShowJobView.as_view(),
            name="show_job"),
    re_path(r'^liste-job/', views.ListJobView.as_view(),
            name="job_list"),
]
