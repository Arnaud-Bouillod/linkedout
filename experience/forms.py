from django import forms
from django.utils.translation import ugettext_lazy as _

from .models import JobOffer, Company

__all__ = [
    'AddJobForm'
]


class AddJobForm(forms.Form):

    address = forms.CharField(max_length=255, label=_("adresse"))
    job = forms.ModelChoiceField(
        queryset=JobOffer.objects.all(), label=_("job"))
    start_date = forms.DateField(label=_("date de début (dd/mm/YYYY)"))
    end_date = forms.DateField(label=_("date de fin (dd/mm/YYYY)"))
    company = forms.ModelChoiceField(
        queryset=Company.objects.all(), label=_("nom de société"))
    contract = forms.CharField(
        max_length=255, label=_("Votre adresse de contrat"))
