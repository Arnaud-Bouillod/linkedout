from django.conf import settings
from django.db import models
from django.utils.translation import ugettext_lazy as _


__all__ = [
    'TrainingCenter', 'Training', 'Company', 'Job', 'JobOffer', 'Project',
]


class StrMixin:

    def __str__(self):
        return self.name


class TrainingCenter(StrMixin, models.Model):

    class Meta:
        verbose_name = _('centre de formation')
        verbose_name_plural = _('centres de formation')
        default_related_name = 'training_center'

    id = models.AutoField(primary_key=True)
    name = models.CharField(verbose_name=_("nom"), max_length=50)
    address = models.CharField(
        verbose_name=_("address"), null=True, blank=True, max_length=50)


class Training(StrMixin, models.Model):

    class Meta:
        verbose_name = _('formation')
        verbose_name_plural = _('formations')
        default_related_name = 'training'

    id = models.AutoField(primary_key=True)
    name = models.CharField(verbose_name=_("nom"), max_length=50)
    employee = models.ForeignKey(
        settings.AUTH_USER_MODEL, models.CASCADE, verbose_name=_("employé"))
    training_center = models.ForeignKey(
        TrainingCenter, models.CASCADE, verbose_name=_("centre de formation"))
    certified = models.BooleanField(verbose_name=_("certifié"), default=False)


class Company(StrMixin, models.Model):

    class Meta:
        verbose_name = _('entreprise')
        verbose_name_plural = _('entreprises')
        default_related_name = 'company'

    id = models.AutoField(primary_key=True)
    name = models.CharField(verbose_name=_("nom"), max_length=50)
    address = models.CharField(
        verbose_name=_("address"), null=True, blank=True, max_length=50)


class Job(StrMixin, models.Model):

    class Meta:
        verbose_name = _('emploi')
        verbose_name_plural = _('emplois')
        default_related_name = 'job'

    id = models.AutoField(primary_key=True)
    name = models.CharField(verbose_name=_("nom"), max_length=50)
    company = models.ForeignKey(
        Company, models.CASCADE, verbose_name=_("entreprise"))


class JobOffer(StrMixin, models.Model):

    class Meta:
        verbose_name = _("offre d'emploi")
        verbose_name_plural = _("offres d'emploi")
        default_related_name = 'job_offer'

    id = models.AutoField(primary_key=True)
    name = models.CharField(verbose_name=_("nom"), max_length=50)
    company = models.ForeignKey(
        Company, models.CASCADE, verbose_name=_("entreprise"))
    description = models.TextField(
        null=True, blank=True, verbose_name=_("description"))


class Project(StrMixin, models.Model):

    class Meta:
        verbose_name = _("projet")
        verbose_name_plural = _("projets")
        default_related_name = 'project'

    id = models.AutoField(primary_key=True)
    name = models.CharField(verbose_name=_("nom"), max_length=50)
    description = models.TextField(
        null=True, blank=True, verbose_name=_("description"))
