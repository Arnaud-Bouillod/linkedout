from django.contrib import admin

from .models import TrainingCenter, Training, Company, JobOffer, Job, Project

__all__ = [
    'TrainingCenterAdmin', 'TrainingAdmin', 'CompanyAdmin', 'JobAdmin',
    'JobOfferAdmin', 'ProjectAdmin',
]


@admin.register(TrainingCenter)
class TrainingCenterAdmin(admin.ModelAdmin):

    fields = ['name']
    search_fields = ['name']


@admin.register(Training)
class TrainingAdmin(admin.ModelAdmin):

    fields = ['name', 'employee', 'training_center', 'certified']
    search_fields = ['name']


@admin.register(Company)
class CompanyAdmin(admin.ModelAdmin):

    fields = ['name', 'address']
    search_fields = ['name']


@admin.register(Job)
class JobAdmin(admin.ModelAdmin):

    fields = ['name', 'company']
    search_fields = ['name']


@admin.register(JobOffer)
class JobOfferAdmin(admin.ModelAdmin):

    fields = ['name', 'company', 'description']
    search_fields = ['name']


@admin.register(Project)
class ProjectAdmin(admin.ModelAdmin):

    fields = ['name', 'description']
    search_fields = ['name']
