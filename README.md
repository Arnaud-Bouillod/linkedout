### pré-requis :

- python>3.5.4

### installation de pip :

`curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py`

`python get-pip.py` (pip doit être mappé sur python>3.5.4)

`pip install --upgrade pip`

### installation du virtualenv :

`(sudo) pip install virtualenv virtualenvwrapper`

`source /usr/local/bin/virtualenvwrapper.sh`

### création du virtualenv :

`mkvirtualenv linkedout`

### lancement du virtualenv :

`workon linkedout`

### installation des dépendances :

`pip install -r requirements.txt`

### lancement du serveur :

`python manage.py runserver`

### Après avoir build le projet Solidity :

- renseigner la variable `FILEJSON_PATH` à la fin du fichier settings
- mettre le path du fichier Json `build/LinkedOut.json`