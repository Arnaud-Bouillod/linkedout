from django.conf.urls import include
from django.contrib import admin
from django.urls import path, re_path

import linkedout.views as views

urlpatterns = [
    path('admin/', admin.site.urls, name="admin"),

    re_path(r'', include('user.urls')),
    re_path(r'', include('experience.urls')),

    re_path(r'^$', views.HomeView.as_view(), name="home"),
    re_path(r'login/$', views.LoginView.as_view(), name="login"),
]
