from django.contrib.auth import login
from django.views.generic import FormView, TemplateView
from django.urls import reverse_lazy

from user.forms import AuthenticationForm


class HomeView(TemplateView):

    template_name = "home.html"


class LoginView(FormView):

    template_name = "login.html"
    form_class = AuthenticationForm
    success_url = reverse_lazy("home")

    def form_valid(self, form):
        login(self.request, form.member)
        return super().form_valid(form)
