from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.utils.translation import ugettext_lazy as _

from .models import Member, StaffMember

__all__ = [
    'MemberAdmin', 'StaffMemberAdmin'
]


@admin.register(Member)
class MemberAdmin(UserAdmin):

    fieldsets = [
        (None, {'fields': [
            'email', 'username', 'password', 'is_staff',
            'date_joined', 'last_login',
        ]}),
        (_("Identité"), {'fields': [
            'civility', 'first_name', 'last_name', 'phone',
            'public_key', 'contract', 'job'
        ]}),
    ]
    search_fields = [
        'email', 'first_name', 'last_name',
    ]
    readonly_fields = [
        'date_joined', 'password',
    ]


@admin.register(StaffMember)
class StaffMemberAdmin(UserAdmin):

    fieldsets = UserAdmin.fieldsets
    search_fields = UserAdmin.search_fields
    readonly_fields = [
        'date_joined', 'password',
    ]
