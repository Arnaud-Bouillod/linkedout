from django.contrib.auth.base_user import BaseUserManager

__all__ = [
    'StaffMemberManager',
]


class StaffMemberManager(BaseUserManager):

    def get_queryset(self):
        return super().get_queryset().filter(is_staff=True)
