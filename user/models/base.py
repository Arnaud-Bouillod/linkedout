from django.contrib.auth.models import AbstractUser
from django.db import models
from django.utils.translation import ugettext_lazy as _

from experience.models import Job
from .managers import StaffMemberManager

__all__ = [
    'Member', 'StaffMember',
]


class Member(AbstractUser):

    class Meta:
        verbose_name = _('membre')
        verbose_name_plural = _('membres')
        default_related_name = 'member'

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['username']

    CIVILITY_NONE = 0
    CIVILITY_MR = 1
    CIVILITY_MRS = 2
    CIVILITY_CHOICES = [
        (CIVILITY_NONE, _("Pas défini")),
        (CIVILITY_MR, _("Monsieur")),
        (CIVILITY_MRS, _("Madame")),
    ]

    id = models.AutoField(primary_key=True)
    is_staff = models.BooleanField(
        default=False, verbose_name=_("est admin"))
    email = models.EmailField(
        max_length=255, unique=True, verbose_name=_("email"))
    first_name = models.CharField(
        max_length=50, blank=True, null=True, verbose_name=_("prénom"))
    last_name = models.CharField(
        max_length=50, blank=True, null=True, verbose_name=_("nom"))
    password = models.CharField(
        max_length=128, verbose_name=_("mot de passe"))
    date_joined = models.DateField(
        auto_now_add=True, verbose_name=_("date d'inscription"))
    last_login = models.DateTimeField(
        blank=True, null=True, verbose_name=_("date de connexion"))
    civility = models.PositiveSmallIntegerField(
        choices=CIVILITY_CHOICES, default=CIVILITY_NONE,
        verbose_name=_("civilité"))
    phone = models.CharField(
        max_length=12, null=True, blank=True, verbose_name=_("téléphone"))
    job = models.ForeignKey(
        Job, models.PROTECT, null=True, blank=True, verbose_name=_("emploi"))
    public_key = models.CharField(
        max_length=255, null=True, blank=True, verbose_name=_("clef publique"))
    contract = models.CharField(
        max_length=255, null=True, blank=True, verbose_name=_("contrat"))

    def __str__(self):
        return "{} {}".format(self.first_name, self.last_name)


class StaffMember(Member):

    class Meta:
        proxy = True
        verbose_name = _("membre de l'équipe")
        verbose_name_plural = _("membres de l'équipe")

    objects = StaffMemberManager()
