from django import forms
from django.contrib.auth.models import Group
from django.contrib.auth.forms import (
    AuthenticationForm as BaseAuthenticationForm,
)
from django.utils.translation import ugettext_lazy as _

from .models import Member

__all__ = [
    'AuthenticationForm'
]


class AuthenticationForm(BaseAuthenticationForm):

    username = forms.CharField(
        max_length=255, widget=forms.EmailInput(),
        label=_("Votre adresse e-mail"))
    password = forms.CharField(
        widget=forms.PasswordInput, label=_("Votre mot de passe"))

    def clean_username(self):
        email = self.cleaned_data.get('username')
        try:
            member = Member.objects.get(email=email)
        except Member.DoesNotExist:
            raise forms.ValidationError(
                'Email et mot de passe invalide', code='invalid_login')
        self.member = member
        return email

    def clean_password(self):
        password = self.cleaned_data.get('password')
        if self.member and not self.member.check_password(password):
            raise forms.ValidationError(
                'Email et mot de passe invalide', code='invalid_login')
        return password


class AddUserForm(forms.Form):

    address = forms.CharField(max_length=255, label=_("adresse"))
    first_name = forms.CharField(max_length=255, label=_("prénom"))
    last_name = forms.CharField(max_length=255, label=_("nom"))
    email = forms.CharField(max_length=255, label=_("email"))
    group = forms.ModelChoiceField(
        queryset=Group.objects.all(), label=_("rôle"))
    contract = forms.CharField(
        max_length=255, label=_("Votre adresse de contrat"))

