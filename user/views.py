from datetime import datetime
import json

from django.conf import settings
from django.urls import reverse_lazy
from django.views.generic import FormView, TemplateView

import web3

from .forms import AddUserForm


class ConnectMixin:

    def connect(self, contract):
        w3 = web3.Web3(web3.HTTPProvider('http://localhost:8545'))
        with open(settings.FILEJSON_PATH) as f:
            info_json = json.load(f)
        abi = info_json['abi']
        contract = web3.main.Web3.toChecksumAddress(contract)
        escrow = w3.eth.contract(address=contract, abi=abi)
        return escrow


class AddUserFormView(ConnectMixin, FormView):

    template_name = "add_user.html"
    form_class = AddUserForm
    success_url = reverse_lazy("home")

    def get_initial(self):
        initial = super().get_initial()
        user = self.request.user
        if user.is_authenticated:
            if user.contract:
                initial['contract'] = user.contract
            if user.public_key:
                initial['address'] = user.public_key
        return initial

    def get_context_data(self, **kwargs):
        user = self.request.user
        kwargs['is_authorized'] = user.is_authenticated and \
                                  user.groups.filter(name="RH").exists()
        return super().get_context_data(**kwargs)

    def post(self, request, *args, **kwargs):
        address = request.POST.get('address', None)
        first_name = request.POST.get('first_name', None)
        last_name = request.POST.get('last_name', None)
        email = request.POST.get('email', None)
        role = request.POST.get('group', None)
        contract_address = request.POST.get('contract', None)

        connect = self.connect(contract_address)

        transac = connect.functions.setUser(address, first_name, last_name, email, int(role)-1).transact(
            {'from': address}
        )
        return super().post(request, *args, **kwargs)

    def string_tobytes32(self, data):
        if len(data) > 32:
            r = data[:32]
        else:
            r = data.ljust(32, '0')
        return bytes(r, 'utf-8')


class ShowUserView(ConnectMixin, TemplateView):

    template_name = "show_user.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        user = self.request.user
        if user.is_authenticated:
            connect = self.connect(user.contract)
            context['call'] = connect.functions.getUser(user.public_key).call()
        return context


class ListUserView(ConnectMixin, TemplateView):

    template_name = "list_user.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        user = self.request.user
        if user.is_authenticated:
            connect = self.connect(user.contract)
            accounts = connect.functions.getAccounts().call()
            context['accounts'] = []
            for account in accounts:
                member = connect.functions.getUser(account).call()
                context['accounts'].append(member)
        return context
