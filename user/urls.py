from django.urls import path, re_path

import user.views as views

urlpatterns = [
    re_path(r'^ajout-utilisateur/', views.AddUserFormView.as_view(),
            name="add_user"),
    re_path(r'^visualisation-utilisateur/', views.ShowUserView.as_view(),
            name="show_user"),
    re_path(r'^liste-utilisateur/', views.ListUserView.as_view(),
            name="user_list"),
]
